from __future__ import print_function
from xml.etree import cElementTree as ET 
import sys, math


def iterparent(tree):
    for parent in tree.getiterator():
        for child in parent:
            yield parent, child

class ContourSVG (object):
	"""
	Simple class to render simple SVG's composed of only polyline elements with points.
	Wnen saving the file, a 'percentage' can be specified to render only a given percentage (from 0.0-1.0)
	of the total number of points
	"""
	def __init__ (self, path):
		self.path = path
		self.numpoints = None

	def open (self):
		# http://stackoverflow.com/questions/8983041/saving-xml-files-using-elementtree
		ET.register_namespace("","http://www.w3.org/2000/svg")
		self.numpoints = 0
		with open(self.path) as f:
			tree = ET.parse(f)
			root = tree.getroot()
		for p in root.findall("{http://www.w3.org/2000/svg}polyline"):
			pts = p.attrib.get("points").strip().split()
			# print len(pts), pts[0], "...", pts[-1]
			self.numpoints += len(pts)
		# print self.numpoints, "points"
		return tree

	def output (self, p=1.0, np=None):
		tree = self.open()
		root = tree.getroot()
		if np == None:
			np = int(self.numpoints * p)
		cp = 0
		rem = []
		for parent, child in iterparent(root):
			if child.tag == "{http://www.w3.org/2000/svg}polyline":
				# remove element if we're done
				if cp >= np:
					# print ("removing element {0} from {1}".format(child, parent), file=sys.stderr)
					# parent.remove(child)
					rem.append((parent, child))
				else:
					pts = child.attrib["points"].strip().split()
					if cp + len(pts) > np:
						# truncate points
						pts = pts[:np-cp]
						child.attrib['points'] = " ".join(pts)
						cp = np
					else:
						# let pass and count the points
						cp += len(pts)
		for p, c in rem:
			p.remove(c)
		return tree

	def save (self, name, p=1.0):
		t = self.output(p)
		with open(name, "wt") as f:
			t.write(f)


if __name__ == "__main__":

	svg = ContourSVG(sys.argv[1])
	# t = svg.output(0, np=2)
	# t.write(sys.stdout)
	svg.save("test.svg", 1.0)

	# print (ET.tostring(t.getroot()))

	# print svg.tree
