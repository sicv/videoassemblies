from PCV.imagesearch.vocabulary import Vocabulary
from argparse import ArgumentParser
import pickle

p = ArgumentParser("Create a vocabulary from a set of .sift input")
p.add_argument("--vocab", default="vocab.pkl")
p.add_argument("--terms", type=int, default=1000)
p.add_argument("--subsample", type=int, default=100)
p.add_argument("--name", default="vocabulary")
p.add_argument("input", nargs="*", default=[])
args = p.parse_args()

voc = Vocabulary(args.name)
print args.input

voc.train(args.input, args.terms, args.subsample)
# voc.train(args.input, 1000, 10)
with open(args.vocab, 'wb') as f:
	pickle.dump(voc, f)
print "vocabulary is:", voc.name, voc.nbr_words
