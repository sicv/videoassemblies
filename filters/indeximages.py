from PCV.imagesearch.imagesearch import Indexer
from argparse import ArgumentParser
from PCV.localdescriptors.sift import read_features_from_file
import os, pickle


p = ArgumentParser()
p.add_argument("input", nargs="*")
p.add_argument("--vocab", default="vocab.pkl")
p.add_argument("--db", default="index.db")
args = p.parse_args()


with open(args.vocab, 'rb') as f:
	voc = pickle.load(f)
indx = Indexer(args.db,voc)
indx.create_tables()

for i in args.input:
	locs, descr = read_features_from_file(i)
	imname = os.path.basename(os.path.splitext(i)[0])
	indx.add_to_index(imname, descr)
indx.db_commit()


