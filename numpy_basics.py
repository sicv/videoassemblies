import numpy as np

r1 = ((255, 255, 255), (0, 0, 0))
r2 = ((0, 0, 0), (128, 128, 128))
i = np.array((r1, r2), 'uint8')

# i.shape => (2, 2, 3)

# b = np.ones((2, 2, 4), 'uint8') * 255
b = np.ones((i.shape[0], i.shape[1], 4), 'uint8') * 255

b[:,:,:-1] = i


print b
print b.shape
