from __future__ import print_function
from argparse import ArgumentParser
import numpy as np 
import cv2, sys
from matplotlib import pyplot as plt 

# tutorial source: http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_feature_homography/py_feature_homography.html

p = ArgumentParser("Stitch one image (loose) onto another (fixed) using features, the loose image is transformed to match the fixed and overlaid")
p.add_argument("--fixed", default="originals/S-20-28-38_1080.jpg", help="the background image")
p.add_argument("--place", default="frames/00007_00-20-02-254.jpg", help="image which is transformed and projected")
p.add_argument("--min-features", type=int, default=10, help="minimum matching features, default: 10")
p.add_argument("--output", default="output.png", help="output image, default: output.png")
p.add_argument("--gray", default=False, action="store_true", help="gray scale output, default: false")
p.add_argument("--verbose", default=False, action="store_true")
args = p.parse_args()

place_image = cv2.imread(args.place,0) # query image
fixed_image = cv2.imread(args.fixed,0) # training

if args.verbose:
    print ("Output: {0} ({1}x{2})".format(args.output, fixed_image.shape[0], fixed_image.shape[1]), file=sys.stderr)
if not args.gray:
     place_image_color = cv2.imread(args.place)
     fixed_image_color = cv2.imread(args.fixed)

# sift = cv2.SIFT() # seems like in opencv3.0.0 this is now...
sift = cv2.xfeatures2d.SIFT_create()
kp1, des1 = sift.detectAndCompute(place_image,None)
kp2, des2 = sift.detectAndCompute(fixed_image, None)

FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees=5)
search_params = dict(checks = 50)

# http://docs.opencv.org/modules/features2d/doc/common_interfaces_of_descriptor_matchers.html?highlight=flannbasedmatcher#flannbasedmatcher
flann = cv2.FlannBasedMatcher(index_params, search_params)
matches = flann.knnMatch(des1,des2,k=2) # order: query, training

good = []
for m, n in matches:
    if m.distance < 0.7*n.distance:
        good.append(m)

if len(good) >= args.min_features:
    # note: reshape -1 means whatever it needs to be to fit all data
    if args.verbose:
        print ("Found {0} matching features".format(len(good)), file=sys.stderr)
    src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1, 1, 2)
    dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1, 1, 2)

    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)

    # http://docs.opencv.org/modules/imgproc/doc/geometric_transformations.html#warpperspective
    if args.gray:
        warped_image = cv2.warpPerspective(place_image, M, fixed_image.shape)
        output_image = cv2.add(fixed_image, warped_image)
    else:
        warped_image = cv2.warpPerspective(place_image_color, M, fixed_image.shape)
        output_image = cv2.add(fixed_image_color, warped_image)
    # plt.imshow(img4, 'gray'),plt.show()
    cv2.imwrite(args.output, output_image)

    # ret, mask = cv2.threshold(img3, 10, 255, cv2.THRESH_BINARY)

    # https://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_core/py_image_arithmetics/py_image_arithmetics.html#image-arithmetics


    # (original tutorial) CODE TO DRAW THE MATCHING POINTS
    # matchesMask = mask.ravel().tolist()
    # h,w=img1.shape
    # pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1, 1, 2)
    # dst = cv2.perspectiveTransform(pts, M)
    # img2 = cv2.polylines(img2, [np.int32(dst)],True,255,3,cv2.LINE_AA)
    # draw_params = dict(matchColor=(255,255,0),
    #                   singlePointColor = None,
    #                   matchesMask = matchesMask, # draw only inliers
    #                   flags=2)
    # img3 = cv2.drawMatches(img1, kp1, img2, kp2, good,None,**draw_params)
    # plt.imshow(img3, 'gray'),plt.show()

else:
    print ("Not enough features match ({0}/{1})".format(len(good), args.min_features))


# http://stackoverflow.com/questions/8205835/stitching-2-images-in-opencv
# http://ramsrigoutham.com/2012/11/22/panorama-image-stitching-in-opencv/